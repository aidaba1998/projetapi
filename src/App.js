import React from 'react'
import {Routes,Route} from 'react-router-dom';
import Home from './home';
import Adduser from './adduser';
import axios from "axios"
import View from './view';
import Edituser from './edituser';
export default function App() {
  const [datas, setDatas] = React.useState([])
  
  const getUser=()=>{
    axios
    .get(`https://cotisation-api-app.herokuapp.com/users`)
    .then(res=>{
      //  console.log(res);
      //  console.log(res.data);
       setDatas(res.data);
      
    
  })
  .catch(function (error){
    console.log(error)
})


}

   
    React.useEffect(() => {
        getUser();
    
      } ,[]);
  return (
    <div> 
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/adduser' element={<Adduser/>}/>
       
        <Route path='/view' element={<View/>}/>
        <Route path='/edituser/:id' element={<Edituser datas={datas}/>}/>
      </Routes>
    </div>
  )
}
