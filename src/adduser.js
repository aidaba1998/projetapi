import React from 'react'
import { useState } from 'react';
import {Link} from 'react-router-dom';
 import axios from 'axios';

export default function Adduser() {

const [firstname,setFirstname]=useState('');
const [lastname,setLastname]=useState('');
const [email,setEmail]=useState('');
const [phone,setPhone]=useState('');
const [address,setAddress]=useState('');
const [profession,setProfession]=useState('');
const [organisation,setOrganisation]=useState('');
const [password,setPassword]=useState('');
const [birthday,setBirthday]=useState('');



 const handleSubmit =(e)=>{
     e.preventDefault();
    
     axios
     .post(`https://cotisation-api-app.herokuapp.com/register`,{firstname,lastname,phone,email,profession,organisation,password,address,birthday})
     .then(res=>{
      //   console.log(res);
      //   console.log(res.data);
     });
 };

  return (
<section className='padding-top:60px;'>
    <div className="container">
      <div className="row">
        <div className="col-md-8">
        <div className="card" >
          <div className="card-header">
            Add uer   <Link  to='/' className='btn btn-success '>retour</Link>
          </div>
     <form  onSubmit={handleSubmit} >
     
      <div className="form-group">
          <label forhtml="firstname"> firstname</label>
      <input type="text" name='firstname' className='form-control'  value={firstname}  onChange={(e)=>setFirstname(e.target.value)} />

      </div>
      <div className="form-group">
          <label forhtml="lastname"> lastname</label>
      <input type="text" name='lastname'className='form-control'  value={lastname} onChange={(e)=>setLastname(e.target.value)} />

      </div>
      <div className="form-group">
         <label forhtml="email"> email</label>
      <input type="text" name='email'className='form-control'  value={email} onChange={(e)=>setEmail(e.target.value)} />

      </div>
      <div className="form-group">
         <label forhtml="profession"> Profession</label>
      <input type="text" name='profession'className='form-control'  value={profession} onChange={(e)=>setProfession(e.target.value)} />

      </div>
      <div className="form-group">
         <label forhtml="date"> Date de naissance</label>
      <input type="date" name='birthday' className='form-control'  value={birthday} onChange={(e)=>setBirthday(e.target.value)} />

      </div>
      <div className="form-group">
         <label forhtml="address"> Adresse</label>
      <input type="text" name='address' className='form-control'  value={address} onChange={(e)=>setAddress(e.target.value)} />

      </div>
      <div className="form-group">
         <label forhtml="prganisation"> Organisation</label>
      <input type="text" name='organisation' className='form-control'  value={organisation} onChange={(e)=>setOrganisation(e.target.value)} />

      </div>
      <div className="form-group">
         <label forhtml="password"> Mot de passse</label>
      <input type="password" name='password' className='form-control'  value={password} onChange={(e)=>setPassword(e.target.value)} />

      </div>
      
      <div className="form-group">
         <label forhtml="phone"> Phone</label>
      <input type="tel" name='phone' className='form-control'  value={phone} onChange={(e)=>setPhone(e.target.value)} />

      </div>
     
     
      <button  to='/'   type="submit" className='btn btn-success'>Add user</button>
     
   </form>
 
 </div>
</div>
             

     
        </div>
      </div>
   
  </section>

  )
}
