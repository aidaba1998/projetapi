
import {Link} from 'react-router-dom';
import axios from 'axios';
import { useEffect, useState } from 'react';



export default function Home() {
const[datas ,setDatas]=useState([]);

// const [firstname,setFirstname]=useState('');
// const [lastname,setLastname]=useState('');
// const [email,setEmail]=useState('');
// const [phone,setPhone]=useState('');
// const [address,setAddress]=useState('');
// const [profession,setProfession]=useState('');
// const [organisation,setOrganisation]=useState('');
// const [password,setPassword]=useState('');
// const [birthday,setBirthday]=useState('');





const getUser=()=>{
    axios
    .get(`https://cotisation-api-app.herokuapp.com/users`)
    .then(res=>{
      //  console.log(res);
      //  console.log(res.data);
       setDatas(res.data);
      
    
  })
  .catch(function (error){
    console.log(error)
})


}

    const deleteUser=  (_id)=>{
      axios
        .delete(`https://cotisation-api-app.herokuapp.com/users/delete/${_id}` )
        .then(res=>{
            // console.log(res)
            // console.log(res.data.id)
          getUser()
           

        })
       
    };
    useEffect(() => {
        getUser();
    
      } ,[]);
      
     
    
    
  return (
    
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-header">
               <Link to="/adduser" className=' btn btn-success'>Add user</Link>
            </div>


 
 <table className="table table-striped">
   <thead>
     <tr>
   
       <th>Firstname</th>
       <th>Lastname</th>
       <th>Email</th>
      
     </tr>
   </thead>
   <tbody>
     
       {datas.map(donnes=>
                <tr  >
                
                     <td>{donnes.firstname}</td>
                     <td>{donnes.lastname}</td>
                     <td>{donnes.email}</td>
                     <td>
                     
                      
                       <Link  to='/' onClick={ ()=> deleteUser(donnes._id)} className='btn btn-danger'  >Delete</Link>
                      <Link  to={`/edituser/${donnes._id}`}  type='button'    className='btn btn-success ' >Edit</Link>

    

               
                    </td>
                </tr>

            
          )}
     
   </tbody>
 </table>
 </div>

 </div>
        </div>

       
      </div>
   


  )
}
