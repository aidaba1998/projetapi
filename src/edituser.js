
import React from 'react'
import { useState, useEffect } from 'react';
import { useParams,Link } from 'react-router-dom';


import axios from 'axios';

export default function Edituser({ datas }) {
   
   const { id } = useParams();
   const currentUser = datas.filter(user => user._id === id)

   const [firstname, setFirstname] = useState(currentUser[0]?.firstname);
   const [lastname, setLastname] = useState(currentUser[0]?.lastname);
   const [email, setEmail] = useState(currentUser[0]?.email);
   const [phone, setPhone] = useState(currentUser[0]?.phone);
   const [address, setAddress] = useState(currentUser[0]?.address);
   const [profession, setProfession] = useState(currentUser[0]?.profession);
   const [organisation, setOrganisation] = useState(currentUser[0]?.organisation);
    const [birthday, setBirthday] = useState(currentUser[0]?.birthday);
   const [user, setUser] = useState({});

   const handleSubmit = (e) => {
      e.preventDefault();

   };

    const edituser = (id) => {
       axios 
       .put(`https://cotisation-api-app.herokuapp.com/users/edit/${id}`, {firstname,lastname,phone,email,profession,organisation,address,birthday})
        .then(res=>{
         // console.log(res);
       })
       .catch(function (error){
            console.log(error)
        })
    }

   const getUser = () => {
      axios
         .get(`https://cotisation-api-app.herokuapp.com/users/${id}`)
         .then(res => {
            // console.log(res);
            // console.log(res.data);
            setUser(res.data);


         })
         .catch(function (error) {
            console.log(error)
         })


   }
   useEffect(() => {
      getUser();
   }, []);


   return (
      <section className='padding-top:60px;'>
         <div className="container">
            <div className="row">
               <div className="col-md-8">
                  <div className="card" >
                     <div className="card-header">
                        Add uer
                     </div>
                     <form onSubmit={handleSubmit} >
                        

                        <div className="form-group">
                           <label forhtml="firstname"> firstname</label>
                           <input type="text" name='firstname' value={firstname} onChange={(e) => setFirstname(e.target.value)} />

                        </div>
                        <div className="form-group">
                           <label forhtml="lastname"> lastname</label>
                           <input type="text" name='lastname' value={lastname}  onChange={(e) => setLastname(e.target.value)} />

                        </div>
                        <div className="form-group">
                           <label forhtml="email"> email</label>
                           <input type="text" name='email' value={email} onChange={(e) => setEmail(e.target.value)} />

                        </div>
                        <div className="form-group">
                           <label forhtml="profession"> Profession</label>
                           <input type="text" name='profession' value={profession} onChange={(e) => setProfession(e.target.value)} />

                        </div>
                        <div className="form-group">
                           <label forhtml="date"> Date de naissance</label>
                           <input type="date" name='birthday' value={birthday} onChange={(e) => setBirthday(e.target.value)} />

                        </div>
                        <div className="form-group">
                           <label forhtml="address"> Adresse</label>
                           <input type="text" name='address' className='form-control' value={address} onChange={(e) => setAddress(e.target.value)} />

                        </div>
                        <div className="form-group">
                           <label forhtml="prganisation"> Organisation</label>
                           <input type="text" name='organisation' value={organisation} onChange={(e) => setOrganisation(e.target.value)} />

                        </div>
                      
                        <div className="form-group">
                           <label forhtml="phone"> Phone</label>
                           <input type="tel" name='phone' value={phone} onChange={(e) => setPhone(e.target.value)} />

                        </div>


                        <Link to='/' onClick={()=>edituser(id)} type='button' className='btn btn-success ' >update</Link>

                     </form>
                  </div>
               </div>


            </div>
         </div>

      </section>


   )
}
